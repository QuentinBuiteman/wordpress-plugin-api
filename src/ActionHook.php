<?php
/**
 * Subscribe to an WP action hook
 *
 * PHP version 7
 *
 * @package WordPressPluginAPI\ActionHook
 * @author  Quentin Buiteman <hello@quentinbuiteman.com>
 * @license https://opensource.org/licenses/MIT MIT
 */

namespace WordPressPluginAPI;

interface ActionHook
{
    /**
     * Subscribes object functions to actions
     *
     * Example returns:
     *     array('action_name' => 'method')
     *     array('action_name' => array('method', $prio))
     *     array('action_name' => array('method', $prio, $args))
     *
     * @return array
     */
    public static function getActions(): array;
}
