<?php
/**
 * Subscribe to an WP filter hook
 *
 * PHP version 7
 *
 * @package WordPressPluginAPI\FilterHook
 * @author  Quentin Buiteman <hello@quentinbuiteman.com>
 * @license https://opensource.org/licenses/MIT MIT
 */

namespace WordPressPluginAPI;

interface FilterHook
{
    /**
     * Subscribe object functions to filters
     *
     * Example returns:
     *     array('filter_name' => 'method')
     *     array('filter_name' => array('method', $prio))
     *     array('filter_name' => array('method', $prio, $args))
     *
     * @return array
     */
    public static function getFilters(): array;
}
