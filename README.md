# WordPress Plugin API
Used for registering your class functions to the WordPress Plugin API.

## Installation
```
composer require quentinbuiteman/wordpress-plugin-api
```

## Usage
Include the `ActionHook` or `FilterHook` in your class and use the `getActions` and/or `getFilters` static functions. The registered function must always be public. You can give a string with the function name, or an array containing the function name, priority and amount of arguments.

## Example Class
```php
use \WordPressPluginAPI\ActionHook;
use \WordPressPluginAPI\FilterHook;

class Setup implements ActionHook, FilterHook
{
    /**
     * Subscribe functions to corresponding actions
     *
     * @return array
     */
    public static function getActions(): array
    {
        return array (
            'init' => 'taxonomies',
        );
    }

    /**
     * Subscribe functions to corresponding filters
     *
     * @return array
     */
    public static function getFilters(): array
    {
        return array (
            'map_meta_cap' => array('mapMetaCap', 10, 4),
        );
    }

    /**
     * Edit taxonomies for custom post types
     *
     * @return void
     */
    public function taxonomies()
    {
        //
    }

    /**
     * Remove capability for anyone else than an admin to remove administrators
     *
     * @param array  $caps User's actual capabilities
     * @param string $cap  Capability
     * @param int    $ID   ID of current user
     * @param array  $args Context
     *
     * @return array
     */
    public function mapMetaCap(array $caps, string $cap, int $ID, array $args): array
    {
        //
    }
}
```
